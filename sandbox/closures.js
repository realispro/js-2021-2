
var counterRef = (function(){
   var counter = 0;
   return {
       adding: ()=>counter+=1,
       decrementing: ()=> counter-=1
   }; 
})();
let tmp = counterRef.counter;
console.log("tmp:" + tmp);

var x = counterRef.adding();
console.log("first:" + x);
x = counterRef.decrementing();
console.log("second:" + x);
x = counterRef.adding();
console.log("third:" + x);


// currying
function add(x){
    return function(y){
        return function(z){
            return x+y+z;
        }
    }
}

let adding10 = add(10);
let adding10and5 = adding10(5);
let result = adding10and5(3);
console.log("result: " + result);

result = add(10)(5)(3);
