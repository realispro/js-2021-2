console.log("Let's start");

let weekend = false; // boolean 
let dayOfWeek = 1; // number

console.log("weekend variable type is: " + typeof weekend);

if(!weekend){
    let msg = 'Today isn\'t Sunday'; // string
    console.log("inside:" + msg);
}

// **** Date
let time = Date.now(); //new Date(2021, 11, 24, 16, 00);
console.log("Time: " + time);

// **** RegExp
let address = "Warsaw 00950, ul. Mysliwiecka 3/5/7, Gdansk 80-123, ul. Dluga 4";
let pattern = "\\d\\d[-]?\\d\\d\\d";

let regExp = /\d\d[-]?\d\d\d/g; //new RegExp(pattern, "g");

let postalCode;
while((postalCode = regExp.exec(address))!=null){
    console.log("postal code: " + postalCode);
}

// **** Arrays
let technologies = ["JavaScript", "HTML", "Python"];
technologies.push("assembler");
technologies[10] = "C#";

let technology = technologies.pop();
console.log("Pop technology: " + technology);

for(let i=0; i<technologies.length; i++){
    console.log("technology: " + technologies[i]);
}

// **** Objects
let project = { // JSON
    "title": "Very crucial project",
    deadline: new Date(2021, 11, 24),
    technologies: technologies,
    devs: ["Asia", "Kasia", "Pawel", "Gawel"],
    info: function(){
        return "Project '" + this.title + "' dealine=" + this.deadline + " team: " + this.devs; 
    }
};
project.budget = 1_000_000;
delete project.deadline;

console.log(project.info());
printObject(project, "xyz", 345);

let tmpObj = {
    title: "Tmp title"
};

console.log("applied: " + project.info.apply(tmpObj));


   function printObject(abc){ // let printObject = function(object){   or:  object =>
       let object = arguments[0];
        for(let propertyName in object){
            let propertyValue = object[propertyName];
            console.log(propertyName + ": " + propertyValue + " of type " + typeof propertyValue)
        }
    }

