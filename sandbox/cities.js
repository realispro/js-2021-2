let cities = [
    {name: 'Los Angeles', population: 3792621},
    {name: 'New York', population: 8175133},
    {name: 'Chicago', population: 2695598},
    {name: 'Houston', population: 2099451},
    {name: 'Philadelphia', population: 1526006}
];

// TODO 1: cities bigger than 3 millions of population
let bigcities = cities.filter(function(element){
    return element.population > 3_000_000;
});
bigcities.forEach(function(e){console.log("duze miasta: "+e.name)});

// TODO 2: calculate population of all cities
let citiesSumOfPopulation = cities.reduce(function(prev, cur){    
    return prev + cur.population;
}, 0);
console.log("\nSUM OF POPULATION:\n----------")
console.log("Population: " + citiesSumOfPopulation);


// TODO 3: sort cities by population, the biggest first
cities.sort((c1,c2)=> c2.population - c1.population);
cities.forEach(function(e){console.log("sorted: " + JSON.stringify(e))})
