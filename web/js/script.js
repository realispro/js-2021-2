window.onload = init;

const ITEM_KEY = "items";
var cntDone = 0;

function init() {
  document.getElementById("logo").src =
    "https://businesstraveller.pl/wp-content/uploads/2018/09/siemens-logo.jpg";
  document.getElementById("new-item-confirm").onclick = addNewItem;
  refreshItems();
}


function addNewItem(){
    let inputField = document.getElementById("new-item-input");
    if(inputField.value.trim().length===0){
        alert("Provide item name first!");
        return;
    }
    let items = getItems();
    items.push(inputField.value);
    updateItems(items);
    refreshItems();
    //consoleOutput("new item has been added");
}

function refreshItems(){
    
    let ul = document.getElementById("to-do-list");         
    ul.innerHTML = '';    
    
    let items = getItems();    
    items.forEach(function(item){
        let li = document.createElement("li");    
        li.appendChild(document.createTextNode(item));
        li.setAttribute("onclick", "doneLi(this)");
        ul.appendChild(li);    
    });
    showCounters();
}
    

function getItems(){
    let items = window.localStorage.getItem(ITEM_KEY);
    if(!items){
        items = [];        
    } else {
        items = JSON.parse(items);
        //consoleOutput("items has been loaded from local storage");
    }
    return items;
}

function updateItems(items){        
    window.localStorage.setItem(ITEM_KEY, JSON.stringify(items));    
}

function consoleOutput(text){
    document.getElementById("console").innerHTML = "<!-- sample comment --><p>" + text + "</p>"
}

function doneLi(el){
    if (el.classList.contains("done")) {
        el.classList.remove("done");
        cntDone--;
    } else {
        el.setAttribute("class", "done");
        cntDone++;
    };
    showCounters();
}

function showCounters() {
    if (getItems().length != 0) {
        consoleOutput("All tasks: " + getItems().length + ", Done tasks: " + cntDone);
    }
    if (getItems().length === cntDone) {
        alert("Moja kongratulacja! Zrobiłeś all :D");
    }
}
