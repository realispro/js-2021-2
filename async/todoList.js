
console.log("before starting fetching items");
let itemListPromise = itemsPromise();
console.log("after starting fetching items");
itemListPromise
    .then(items=>items.forEach(i => console.log("item:" + JSON.stringify(i))))
    .catch(msg=>console.log("error encountered:" + msg));
console.log("after registering resolve handler");



function itemsPromise(){
    let handler = function(resolve, reject){
        setTimeout(()=>reject("random error"), 5000);
    };
    return new Promise(handler);
}


function getItems() {
    let items = [
        {name: "HTML", done: true},
        {name: "CSS", done: true},
        {name: "JS", done: true},
        {name: "NPM", done: false}
    ];

    return items;
}