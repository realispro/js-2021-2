class Developer {  
    constructor(name, level=1, ...skills){
      this.name = name;
      this.level = level;
      this.skills = skills;
    }
  
    visitCard(){
      return "Developer " + this.name + " at level " + this.level; 
    }
}

export { Developer }