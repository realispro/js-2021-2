import { Developer } from './Developer.js'

class FrontendDeveloper extends Developer{

    constructor(name, level, style, ...skills){
        super(name, level, skills);
        this.style = style;
    }

    visitCard(){
      return "@@@" + super.visitCard() + "@@@";
    }

}

export { FrontendDeveloper }