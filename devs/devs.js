import { Developer } from "./Developer.js";
import { FrontendDeveloper } from "./FrontendDeveloper.js";


try {
  let dev1 = new Developer("Asia", 4, "JS", "HTML", "CSS");
  let dev2 = new FrontendDeveloper("Kasia", 2, "Modern", "Python", "JS");
  let dev3 = new Developer("Pawel", 4, "Java", "JS", "HTML", "CSS");
  var dev4 = new Developer("Gawel");

  Developer.prototype.typeDescription = "Developer type";
  dev2.typeDescription = "dev2 type";

  console.log(dev4.visitCard());
  
  var devs = [dev1, dev2, dev3, dev4];
} catch (err) {
  console.log("error catched: " + err);
  //var devs = [];
  throw err;
  //return;
} finally {
  console.log("in finally block");
}

//let devs = [dev1,dev2,dev3,dev4];
console.log(JSON.stringify(devs));

/*
for(let i=0; i<devs.length; i++){
    console.log(devs[i]);
}*/

/*let forEachConsumer = function(element){
    console.log(element);
}*/
devs.forEach( (element, index) => console.log("dev" + index + ":" + JSON.stringify(element)));

/*let seniors = [];
for(let i=0;i<devs.length;i++){
    if(devs[i].level>=3){
        seniors.push(devs[i]);
    }
}*/
/*let filterPredicate = function(element){
    return element.level>=3;
};*/
let seniors = devs.filter(element => element.level >= 3 );


seniors.forEach(e => console.log(
    `senior: 
    ${e.name},
    ${e.level}`
    ));

/*let names = [];
for(let i=0; i<devs.length; i++){
    names.push(devs[i].name);    
}*/
// let mapFunction = function(element){
//     return element.name;
// };
let names = devs.map(element => element.name);
names.forEach(n => console.log("name: " + n));

let reducer = (previousValue, dev) => {
  if (typeof previousValue != "number" && previousValue.level != undefined) {
    previousValue = previousValue.level;
  }
  return previousValue + dev.level;
};
let sumLevel = devs.reduce(reducer, 0);
console.log("dev average level:" + sumLevel / devs.length);

devs.sort( (d1, d2) => {
  let levelDiff = d2.level - d1.level;
  if (levelDiff === 0) {
    return d2.skills.length - d1.skills.length;
  } else {
    return levelDiff;
  }
});
devs.forEach( e => console.log("sorted:" + JSON.stringify(e)));

// Factory method - creational pattern
function developer(name, level) {
  if (level === undefined) {
    throw "Level undefined for a developer " + name;
  }

  let skills = [];
  for (let i = 2; i < arguments.length; i++) {
    skills.push(arguments[i]);
  }

  return {
    name: name,
    level: level,
    skills: skills,
  };
}


/*
function Developer(name, level=1, ...skills){
  this.name = name;
  this.level = level;
  this.skills = skills;
}

Developer.prototype.visitCard = function(){
  return "Developer " + this.name + " at level " + this.level; 
}
*/


